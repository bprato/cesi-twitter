package com.cesi.twitter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "TWEETS")
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Tweet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TWEET_ID")
    private Long id;

    @Column(name = "TWEET_TEXT")
    private String text;

    @Column(name = "POST_DATE")
    private Date postDate;

    @Column(name = "IMAGE")
    private String image;

    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private User author;

    @ManyToOne
    @JoinColumn(name = "PARENT_TWEET")
    private Tweet parentTweet;

    @OneToMany(mappedBy = "parentTweet")
    private List<Tweet> responses;

    @ManyToMany
    @JoinTable(name = "LIKING_USERS_TWEETS",
            joinColumns = {@JoinColumn(name = "TWEET_ID", referencedColumnName = "TWEET_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")}
    )
    private List<User> likingUsers;

    @ManyToMany
    @JoinTable(name = "TWEETS_HASHTAGS",
            joinColumns = {@JoinColumn(name = "TWEET_ID", referencedColumnName = "TWEET_ID")},
            inverseJoinColumns = {@JoinColumn(name = "HASHTAG_ID", referencedColumnName = "HASHTAG_ID")}
    )
    private List<HashTag> hashtags;
}
