package com.cesi.twitter.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ben on 26/06/2018.
 *
 * @author <a href="mailto:pratobenjamin@gmail.com">Benjamin Prato</a>
 */

@Entity
@Table(name = "NOTIFICATIONS")
@Data
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class Notification implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "NOTIFICATION_ID")
    private Long id;

    @Column(name = "NOTIFICATION_TEXT")
    private String text;

    @Column(name = "IS_READ")
    private Boolean read;

    @ManyToOne
    @JoinColumn(name = "RECEIVER_ID")
    private User user;
}
